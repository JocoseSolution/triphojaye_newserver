﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="Bankdetails.aspx.cs" Inherits="SprReports_Accounts_Bankdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <br />
    <br />
    <section class="">

        
        
		<div class="container" style="margin-top:65px;">
         
			<div class="row">
			
                  <div class="col-md-6">
				  
				  <div class="row">
                	<div class="col-md-12 col-sm-5">
                    
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/ICICI-Bank-PNG-Icon-715x715.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">ICICI Bank</h4>
							<p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	TRIP HOJAYE PVT LTD<br />BANK NAME        :	ICICI BANK LTD 1<br />ACCOUNT NO       :	149005001268<br />IFSC code        :			ICIC0001490</p>
						</div>
					</div>
					</div>
                       
				</div>

<div class="row">
                  <div class="col-md-12 col-sm-5">
                    
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/Axis-Bank-Logo.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">AXIS BANK </h4>
							<p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	TRIP HOJAYE PVT LTD<br />BANK NAME        :	AXIS BANK<br />ACCOUNT NO       :	920020055058989<br />IFSC code        :			UTIB0001153</p>
						</div>
					</div>
                     </div>  
				</div>
				
				<div class="row">
				     <div class="col-md-12 col-sm-5">
                    
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/com.kotakprime.jpg" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">KOTAK MAHINDRA BANK </h4>
							<p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	TRIP HOJAYE PVT LTD<br />BANK NAME        :	KOTAK MAHINDRA BANK<br />ACCOUNT NO       :	8989898999<br />IFSC code        :			KKBK0001353</p>
						</div>
					</div>
                       </div>
				</div>
				</div>


<div class="col-md-4">
<div class="row">
				<div class="col-md-12" col-sm-5>
				<img src="../../Images/Bank/WhatsApp Image 2021-03-27 at 5.47.32 PM.jpeg" style="width:100%;"/>
				</div>
			</div>
				</div>
			</div>
		</div>

      
     <br />
     <br />
     <br />
        

	</section>



    

</asp:Content>

