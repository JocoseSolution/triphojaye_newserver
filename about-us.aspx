﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="about-us.aspx.vb" Inherits="about_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="Advance_CSS/css/bootstrap.css" rel="stylesheet" />
    <link href="Advance_CSS/css/styles.css" rel="stylesheet" />

    <div class="theme-hero-area theme-hero-area-half">
        <div class="theme-hero-area-bg-wrap">

            <div class="theme-hero-area-bg" style="background-image: url(https://bollyhollybaba.com/wp-content/uploads/2019/03/business-background-picture-wide-hd-cool-wallpaper-85481.jpg);"></div>
            <%--<div class="theme-hero-area-mask theme-hero-area-mask-half"></div>--%>
            <div class="theme-hero-area-inner-shadow"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 theme-page-header-abs">
                        <div class="theme-page-header theme-page-header-lg">
                            <h1 class="theme-page-header-title">Discover Triphojaye</h1>
                            <p class="theme-page-header-subtitle">The Story of Our Company</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="theme-page-section theme-page-section-xl theme-page-section-gray">
        <div class="container">

            <p style="text-align: justify">
                Trip Hojaye Private Limited has emerged as India's Leading B2B Travel Portal since its establishment in 2020 Registered address at"SHOP NO 5 HEENA ASHISH WING D KALYAN Thane MH 421301 IN"  
we are enabling our partners to serve their customers efficiently, with the right pricing/Best Deal and inventory.
            </p>

            <p style="text-align: justify">
                Ever since our introduction, we have been delivering a wide range of travel solutions for the travel agents and tour operators across India including
metro cities like New Delhi, Mumbai, Chennai and Kolkata to help them take their business to new heights. Our services include
Hotel and Flight Train Booking Dynamic/Reasonable Packages.
            </p>

            <p style="text-align: justify">The continuous support from our partners and hard work of our team has earned us many prestigious `like Best Online B2B Travel Portal.</p>

        </div>
    </div>

</asp:Content>

